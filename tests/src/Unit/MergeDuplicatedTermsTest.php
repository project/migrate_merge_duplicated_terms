<?php

namespace Drupal\Tests\migrate_merge_duplicated_terms\Unit;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate\Plugin\migrate\id_map\Sql;
use Drupal\migrate\Plugin\Migration;
use Drupal\migrate\Row;
use Drupal\migrate_merge_duplicated_terms\EventSubscriber\MergeDuplicatedTermsEventSubscriber;
use Drupal\migrate_merge_duplicated_terms\Plugin\migrate\process\MergeDuplicatedTerms;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;
use Drupal\migrate_plus\Plugin\migrate\process\SingleValue;
use Drupal\taxonomy\Entity\Term;
use Drupal\taxonomy\TermStorage;
use Drupal\Tests\UnitTestCase;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * Tests the merge_duplicated_terms process plugin event subscriber.
 *
 * @coversDefaultClass \Drupal\migrate_merge_duplicated_terms\EventSubscriber\MergeDuplicatedTermsEventSubscriber
 *
 * @group migrate_plus
 */
class MergeDuplicatedTermsTest extends UnitTestCase {

  use ProphecyTrait;

  /**
   * The entity type manager prophecy.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  private $entityTypeManagerProphecy;

  /**
   * The term storage prophecy.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  private $termStorageProphecy;

  /**
   * The term prophecy.
   *
   * @var \Drupal\taxonomy\Entity\Term
   */
  private $termProphecy;

  /**
   * The migrate prepare row event prophecy.
   *
   * @var \Drupal\migrate_plus\Event\MigratePrepareRowEvent
   */
  private $eventProphecy;

  /**
   * The migration prophecy.
   *
   * @var \Drupal\migrate\Plugin\Migration
   */
  private $migrationProphecy;

  /**
   * The migrate row prophecy.
   *
   * @var \Drupal\migrate\Row
   */
  private $rowProphecy;

  /**
   * The migrate SQL Prophecy.
   *
   * @var \Drupal\migrate\Plugin\migrate\id_map\Sql
   */
  private $sqlProphecy;

  /**
   * The merge_duplicated_terms process plugin prophecy.
   *
   * @var \Drupal\migrate_merge_duplicated_terms\Plugin\migrate\process\MergeDuplicatedTerms
   */
  private $mergeDuplicatedTermsProphecy;

  /**
   * The migrate single value prophecy.
   *
   * @var \Drupal\migrate_plus\Plugin\migrate\process\SingleValue
   */
  private $singleValueProphecy;

  /**
   * The merge duplicated terms event subscriber.
   *
   * @var \Drupal\migrate_merge_duplicated_terms\EventSubscriber\MergeDuplicatedTermsEventSubscriber
   */
  private $mergeDuplicatedTermsEventSubscriber;

  /**
   * {@inheritdoc}
   */
  public function setUp(): void {
    parent::setUp();

    $this->entityTypeManagerProphecy = $this->prophesize(EntityTypeManagerInterface::class);
    $this->termStorageProphecy = $this->prophesize(TermStorage::class);
    $this->termProphecy = $this->prophesize(Term::class);
    $this->eventProphecy = $this->prophesize(MigratePrepareRowEvent::class);
    $this->migrationProphecy = $this->prophesize(Migration::class);
    $this->rowProphecy = $this->prophesize(Row::class);
    $this->sqlProphecy = $this->prophesize(Sql::class);
    $this->mergeDuplicatedTermsProphecy = $this->prophesize(MergeDuplicatedTerms::class);
    $this->singleValueProphecy = $this->prophesize(SingleValue::class);

    $this->entityTypeManagerProphecy->getStorage('taxonomy_term')
      ->willReturn($this->termStorageProphecy->reveal());

    $this->mergeDuplicatedTermsEventSubscriber = new MergeDuplicatedTermsEventSubscriber(
      $this->entityTypeManagerProphecy->reveal()
    );
  }

  /**
   * @covers ::prepareRow
   */
  public function testNotTaxonomyTermDestination(): void {
    $this->eventProphecy
      ->getMigration()
      ->willReturn($this->migrationProphecy->reveal());
    $this->migrationProphecy->getDestinationConfiguration()
      ->willReturn([
        'plugin' => 'entity:node',
      ]);
    $this->eventProphecy
      ->getRow()
      ->shouldNotBeCalled();

    $this->mergeDuplicatedTermsEventSubscriber
      ->prepareRow($this->eventProphecy->reveal());
  }

  /**
   * @covers ::prepareRow
   */
  public function testNotOurProcessPlugin(): void {
    $this->eventProphecy
      ->getMigration()
      ->willReturn($this->migrationProphecy->reveal());
    $this->migrationProphecy
      ->getDestinationConfiguration()
      ->willReturn([
        'plugin' => 'entity:taxonomy_term',
        'default_bundle' => 'foo',
      ]);
    $this->eventProphecy
      ->getRow()
      ->shouldBeCalled()
      ->willReturn($this->rowProphecy->reveal());
    $this->migrationProphecy
      ->getIdMap()
      ->shouldBeCalled()
      ->willReturn($this->sqlProphecy->reveal());
    $this->migrationProphecy
      ->getProcessPlugins()
      ->shouldBeCalled()
      ->willReturn([
        'name' => [
          $this->singleValueProphecy->reveal(),
        ],
      ]);
    $this->mergeDuplicatedTermsProphecy
      ->getSourceFieldName()
      ->shouldNotBeCalled();

    $this->mergeDuplicatedTermsEventSubscriber
      ->prepareRow($this->eventProphecy->reveal());
  }

  /**
   * @covers ::prepareRow
   */
  public function testMigrateThisTerm(): void {
    $this->eventProphecy
      ->getMigration()
      ->willReturn($this->migrationProphecy->reveal());
    $this->migrationProphecy
      ->getDestinationConfiguration()
      ->willReturn([
        'plugin' => 'entity:taxonomy_term',
        'default_bundle' => 'foo',
      ]);
    $this->eventProphecy
      ->getRow()
      ->shouldBeCalled()
      ->willReturn($this->rowProphecy->reveal());
    $this->migrationProphecy
      ->getIdMap()
      ->shouldBeCalled()
      ->willReturn($this->sqlProphecy->reveal());
    $this->migrationProphecy
      ->getProcessPlugins()
      ->shouldBeCalled()
      ->willReturn([
        'name' => [
          $this->mergeDuplicatedTermsProphecy->reveal(),
        ],
      ]);
    $this->mergeDuplicatedTermsProphecy
      ->getSourceFieldName()
      ->shouldBeCalled()
      ->willReturn('name');
    $this->rowProphecy
      ->getSource()
      ->shouldBeCalled()
      ->willReturn(['name' => 'bar']);

    $this->termStorageProphecy
      ->loadByProperties([
        'name' => 'bar',
        'vid' => 'foo',
      ])
      ->shouldBeCalled()
      ->willReturn([]);

    $this->assertNull(
      $this->mergeDuplicatedTermsEventSubscriber
        ->prepareRow($this->eventProphecy->reveal())
    );
  }

  /**
   * @covers ::prepareRow
   */
  public function testDoNotMigrateThisTerm(): void {
    $this->eventProphecy
      ->getMigration()
      ->willReturn($this->migrationProphecy->reveal());
    $this->migrationProphecy
      ->getDestinationConfiguration()
      ->willReturn([
        'plugin' => 'entity:taxonomy_term',
        'default_bundle' => 'foo',
      ]);
    $this->eventProphecy
      ->getRow()
      ->shouldBeCalled()
      ->willReturn($this->rowProphecy->reveal());
    $this->migrationProphecy
      ->getIdMap()
      ->shouldBeCalled()
      ->willReturn($this->sqlProphecy->reveal());
    $this->migrationProphecy
      ->getProcessPlugins()
      ->shouldBeCalled()
      ->willReturn([
        'name' => [
          $this->mergeDuplicatedTermsProphecy->reveal(),
        ],
      ]);
    $this->mergeDuplicatedTermsProphecy
      ->getSourceFieldName()
      ->shouldBeCalled()
      ->willReturn('name');
    $this->rowProphecy
      ->getSource()
      ->shouldBeCalled()
      ->willReturn(['name' => 'bar']);

    $this->termStorageProphecy
      ->loadByProperties([
        'name' => 'bar',
        'vid' => 'foo',
      ])
      ->shouldBeCalled()
      ->willReturn([
        $this->termProphecy->reveal(),
      ]);
    $this->expectException(MigrateSkipRowException::class);

    $this->mergeDuplicatedTermsEventSubscriber
      ->prepareRow($this->eventProphecy->reveal());
  }

}
