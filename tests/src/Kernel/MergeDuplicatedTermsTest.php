<?php

namespace Drupal\Tests\migrate_merge_duplicated_terms\Kernel;

use Drupal\Core\Language\LanguageInterface;
use Drupal\KernelTests\KernelTestBase;
use Drupal\migrate\MigrateExecutable;
use Drupal\migrate\MigrateMessageInterface;

/**
 * Tests the merge_duplicated_terms process plugin event subscriber.
 *
 * @coversDefaultClass \Drupal\migrate_merge_duplicated_terms\EventSubscriber\MergeDuplicatedTermsEventSubscriber
 *
 * @group migrate_plus
 */
class MergeDuplicatedTermsTest extends KernelTestBase implements MigrateMessageInterface {

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'migrate_merge_duplicated_terms',
    'migrate_plus',
    'migrate',
    'user',
    'system',
    'taxonomy',
    'field',
    'text',
  ];

  /**
   * The vocabulary id.
   *
   * @var string
   */
  protected $vocabulary = 'fruit';

  /**
   * The migration plugin manager.
   *
   * @var \Drupal\migrate\Plugin\MigrationPluginManager
   */
  protected $migrationPluginManager;

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * The taxonomy vocabulary storage.
   *
   * @var \Drupal\taxonomy\VocabularyStorage
   */
  protected $vocabularyStorage;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $entity_type_manager = \Drupal::service('entity_type.manager');
    $this->vocabularyStorage = $entity_type_manager->getStorage('taxonomy_vocabulary');
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');

    $this->installEntitySchema('user');
    $this->installEntitySchema('taxonomy_vocabulary');
    $this->installEntitySchema('taxonomy_term');

    // Create a vocabulary.
    $vocabulary = $this->vocabularyStorage->create([
      'name' => $this->vocabulary,
      'description' => $this->vocabulary,
      'vid' => $this->vocabulary,
      'langcode' => LanguageInterface::LANGCODE_NOT_SPECIFIED,
    ]);
    $vocabulary->save();

    $this->migrationPluginManager = \Drupal::service('plugin.manager.migration');
  }

  /**
   * @covers ::prepareRow
   */
  public function testMigratingDuplicatedTerms(): void {
    $definition = [
      'source' => [
        'plugin' => 'embedded_data',
        'data_rows' => [
          [
            'id' => 1,
            'term_name' => 'Apples',
          ],
          [
            'id' => 2,
            'term_name' => 'Apples',
          ],
        ],
        'ids' => [
          'id' => ['type' => 'integer'],
        ],
      ],
      'process' => [
        'tid' => 'id',
        'name' => [
          'plugin' => 'merge_duplicated_terms',
          'source' => 'term_name',
        ],
      ],
      'destination' => [
        'plugin' => 'entity:taxonomy_term',
        'default_bundle' => $this->vocabulary,
      ],
    ];
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $this->migrationPluginManager->createStubMigration($definition);
    $migrationExecutable = (new MigrateExecutable($migration, $this));
    $migrationExecutable->import();
    $terms = $this->termStorage->loadByProperties([
      'vid' => $this->vocabulary,
      'name' => 'Apples',
    ]);
    // Expect only one term migrated.
    $this->assertEquals(1, count($terms));

    // Expect destination id to be equal to 1 for term id 2.
    $id_map = $migration->getIdMap();
    $destination_ids = $id_map->lookupDestinationIds([2]);
    $this->assertEquals(1, reset($destination_ids[0]));
  }

  /**
   * {@inheritdoc}
   */
  public function display($message, $type = 'status'): void {
    $this->assertTrue($type == 'status', $message);
  }

}
