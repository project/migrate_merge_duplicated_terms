# Migrate Plus - Merge duplicated terms

CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Configuration
 * Maintainers

INTRODUCTION
------------

This module provides a plugin which dedupe taxonomy terms merging them into one
during a migration.

If you have duplicated terms in a vocabulary you are about to migrate, this
plugin will only migrate the first occurrence of a term, skip the other rows
with the same name but it will map the migrated term id with every source id
which has the same name (so if you have other content tagged with these
terms id, they will be attached to the migrated term id when you will use
the migration_lookup).

If you want to merge different terms name into one during your migration,
you just need to rename those terms in your source (database, csv, json, etc.)
so that they have the same name, then use this plugin during the migration.

[Use case example](https://drupal.stackexchange.com/questions/193389/migration-bringing-in-duplicate-taxonomy-terms)

 * For a full description of the module, visit the project page:
   https://www.drupal.org/project/migrate_merge_duplicated_terms

 * To submit bug reports and feature suggestions, or track changes:
   https://www.drupal.org/project/issues/migrate_merge_duplicated_terms

REQUIREMENTS
------------

This module requires the following modules:

 * Taxonomy (Core module) must be enabled
 * [Migrate plus](https://www.drupal.org/project/migrate_plus)

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. Visit
   https://www.drupal.org/node/1897420 for further information.

CONFIGURATION
-------------

The module has no menu or modifiable settings. There is no configuration.
Add the following lines in your migration yml file and just replace
"source_field" & "vocabulary_id" by the values you need:

```
process:
  name:
    plugin: merge_duplicated_terms
    source: source_field
destination:
  plugin: 'entity:taxonomy_term'
  default_bundle: vocabulary_id
```

MAINTAINERS
-----------

Current maintainers:
 * Maxime Roux (MacSim) - https://www.drupal.org/u/macsim
