<?php

namespace Drupal\migrate_merge_duplicated_terms\EventSubscriber;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\migrate\MigrateSkipRowException;
use Drupal\migrate_merge_duplicated_terms\Plugin\migrate\process\MergeDuplicatedTerms;
use Drupal\migrate_plus\Event\MigrateEvents;
use Drupal\migrate_plus\Event\MigratePrepareRowEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * No Duplicate Terms Event Subscriber.
 */
class MergeDuplicatedTermsEventSubscriber implements EventSubscriberInterface {

  /**
   * The taxonomy term storage.
   *
   * @var \Drupal\taxonomy\TermStorage
   */
  protected $termStorage;

  /**
   * Construct the event subscriber.
   *
   * @param \Drupal\Core\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(EntityTypeManagerInterface $entity_type_manager) {
    $this->termStorage = $entity_type_manager->getStorage('taxonomy_term');
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents() {
    return [
      MigrateEvents::PREPARE_ROW => 'prepareRow',
    ];
  }

  /**
   * Skip duplicated rows but save the id to the mapping.
   *
   * @param \Drupal\migrate_plus\Event\MigratePrepareRowEvent $event
   *   The migrate prepare row event.
   */
  public function prepareRow(MigratePrepareRowEvent $event) {
    /** @var \Drupal\migrate\Plugin\Migration $migration */
    $migration = $event->getMigration();
    $destination_definition = $migration->getDestinationConfiguration();

    if ($destination_definition['plugin'] != 'entity:taxonomy_term') {
      return;
    }

    /** @var \Drupal\migrate\Row $row */
    $row = $event->getRow();
    /** @var \Drupal\migrate\Plugin\migrate\id_map\Sql $id_map */
    $id_map = $migration->getIdMap();
    $process_plugins = $migration->getProcessPlugins();

    foreach ($process_plugins['name'] as $process_plugin) {
      if (!($process_plugin instanceof MergeDuplicatedTerms)) {
        continue;
      }

      $source_field = $process_plugin->getSourceFieldName();
      $term_name = $row->getSource()[$source_field];

      $properties = [
        'name' => $term_name,
        'vid' => $destination_definition['default_bundle'],
      ];

      $terms = $this->termStorage->loadByProperties($properties);
      if (!empty($terms)) {
        /** @var \Drupal\taxonomy\Entity\Term $term */
        $term = reset($terms);
        $id_map->saveIdMapping($row, [$term->id()]);
        throw new MigrateSkipRowException('', FALSE);
      }
    }
  }

}
