<?php

namespace Drupal\migrate_merge_duplicated_terms\Plugin\migrate\process;

use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;

/**
 * Ensures a term won't be duplicated but mapping will be kept.
 *
 * @MigrateProcessPlugin(
 *   id = "merge_duplicated_terms",
 * )
 *
 * @code
 *   process:
 *     name:
 *       plugin: merge_duplicated_terms
 *       source: source_field
 *   destination:
 *     plugin: 'entity:taxonomy_term'
 *     default_bundle: vocabulary_id
 * @endcode
 *
 * The process does nothing here but the definition of the plugin
 * will trigger the event.
 *
 * @see Drupal\migrate_merge_duplicated_terms\EventSubscriber\MergeDuplicatedTermsEventSubscriber
 */
class MergeDuplicatedTerms extends ProcessPluginBase {

  /**
   * Return source field name.
   *
   * @return string
   *   The source field name.
   */
  public function getSourceFieldName() {
    return $this->configuration['source'];
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    return $value;
  }

}
